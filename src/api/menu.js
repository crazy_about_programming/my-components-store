let api='/api'
const menuApi  = {
  resourcesList:api+'/resources/list',//获取菜单列表
  listTree:api+'/resources/listAllTree',//获取菜单树
  listResourceByMenuId:api+'/resources/listResourceByMenuId',//选择菜单树的节点获取下级的菜单列表信息
  resourcesAdd:api+'/resources/add',//菜单新增
  resourcesEdit:api+'/resources/edit',//菜单修改
  resourcesRemove:api+'/resources/remove',//菜单删除，与批量删除
  resourcesGet:api+'/resources/get'//根据id获取菜单信息
}

export default menuApi
