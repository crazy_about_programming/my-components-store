let api = '/api/py-user'
let APISelect2 = '/api/py-organ'
const partyDyMgrApi = {
    /**
     * @param {partyDyMgrApi} 党员信息管理
     */
    queryOrgan: api + '/eacorgan/organTree',//根据组织id和组织名称查询组织
    delUserInfo: api + '/eacUser/delete',//根据用户id删除用户信息/72983
    delAllUserInfo: api + '/eacUser/deletes',//根据用户id批量删除用户
    queryUserInfo: api + '/eacUser/getByUserId',//根据用户id查询用户信息
    advancedQuery: api + '/eacUser/getListByKeyWord',//多条件查询党员
    getUserByOrganCode: api + '/eacUser/getUserByOrganCode',//根据orgCode获取本级党员列表
    getUserByOrganId: api + '/eacUser/getUserByOrganId',//根据orgId获取本级党员列表
    getUserByOrLastCode: api + '/eacUser/getUserByOrLastCode',//根据orgCode获取本级党员以及下级列表
    getUserByOrLastganId: api + '/eacUser/getUserByOrLastganId',//根据orgId获取本级党员以及下级列表
    addDy: api + '/eacUser/insert',//新增党员
    editDy: api + '/eacUser/update',//修改党员信息
    OrganThroughDy: api + '/eacUser/updateOrgan',//组织转接党员
    // <********工作岗位**********>
    addUpdateUser: APISelect2 + '/dj/eac/eacusergzgw/editGzgwUser',//新增/修改工作岗位
    delUser: APISelect2 + '/dj/eac/eacusergzgw/delGzgw',//删除一个工作岗位
    gzgwList: APISelect2 + '/dj/eac/eacusergzgw/gzgwList',//当前党务工作者工作岗位集合
    gzgwDetail: APISelect2 + '/dj/eac/eacusergzgw/gzgwDetail',//查看一个工作岗位详情
    //<********党内职务**********>
    addUpdateDnzwUser: APISelect2 + '/dj/eac/eacuserdnzw/editDnzwUser',//新增或修改党内职务
    dnzwList: APISelect2 + '/dj/eac/eacuserdnzw/dnzwList',//当前党务工作者党内职务集合
    dnzwDetail: APISelect2 + '/dj/eac/eacuserdnzw/dnzwDetail',//查看一个党内职务详情
    delDnzw: APISelect2 + '/dj/eac/eacuserdnzw/delDnzw',//删除一个党内职务
    //<********行政职务**********>
    addUpdateXzzwUser: APISelect2 + '/dj/eac/eacuserxzzw/editXzzwUser',//新增/修改行政职务
    xzzwList: APISelect2 + '/dj/eac/eacuserxzzw/xzzwList',//当前党务工作者行政职务集合
    xzzwDict: APISelect2 + '/dj/eac/eacuserxzzw/xzzwDict',//行政职务数据字典
    xzzwDetail: APISelect2 + '/dj/eac/eacuserxzzw/xzzwDetail',//查看一个行政职务详情
    delXzzw: APISelect2 + '/dj/eac/eacuserxzzw/delXzzw',//删除一个行政职务
    //<********技术职务**********>
    addUpdateJszwUser: APISelect2 + '/dj/eac/eacuserjszw/editJszwUser',//新增/修改技术职务
    jszwList: APISelect2 + '/dj/eac/eacuserjszw/jszwList',//当前党务工作者技术职务集合
    jszwDetail: APISelect2 + '/dj/eac/eacuserjszw/jszwDetail',//查看一个技术职务详情
    delJszw: APISelect2 + '/dj/eac/eacuserjszw/delJszw',//删除一个技术职务
    //<********家庭成员**********>
    addUpdateJtcyUser: APISelect2 + '/dj/eac/eacuserjtcy/editJtcyUser',//新增或修改家庭成员
    jtcyList: APISelect2 + '/dj/eac/eacuserjtcy/jtcyList',//当前党务工作者家庭成员集合
    jtcyDetail: APISelect2 + '/dj/eac/eacuserjtcy/jtcyDetail',//查看一个家庭成员详情
    delJtcy: APISelect2 + '/dj/eac/eacuserjtcy/delJtcy',//删除一个家庭成员
    jtcyDicts: APISelect2 + '/dj/eac/eacuserjtcy/jtcyDicts',//家庭成员相关数据字典
    //<********学历学位**********>
    addUpdateXlxwUser: APISelect2 + '/dj/eac/eacuserxlxw/editXlxwUser',//新增或修改学位
    delXlxw: APISelect2 + '/dj/eac/eacuserxlxw/delXlxw',//删除一个学历学位
    xlxwList: APISelect2 + '/dj/eac/eacuserxlxw/xlxwList',//当前党务工作者学历学位集合
    xlxwDetail: APISelect2 + '/dj/eac/eacuserxlxw/xlxwDetail',//学历学位详情

    /**
     * @param {historyDyMgr} 历史党员管理
     */
    delHistoryDy: api + '/userHistory/deleteHistory',//删除历史党员
    queryHistoryDy: api + '/userHistory/getHistoryById',//查询单个历史党员
    queryHistoryDyAll: api + '/userHistory/selectHistoryByWord',//查询历史党员集合
    restoreHistoryDy: api + '/userHistory/updateHistoryUser',//还原(修改历史党员为正常党员)

    /**
    * @param {partyAwardMgr} 党员奖励管理
    */
    queryByUserName: api + '/eacUserReward/queryByUserName',//根据用户名称分页查询奖励
    updateRewordStatus: api + '/eacUserReward/updateRewordStatus',//批量撤销奖励
    insert: api + '/eacUserReward/insert',//新增奖励党员
    update: api + '/eacUserReward/update',//修改奖励党员
    delete: api + '/eacUserReward/delete',//批量删除
    getById: api + '/eacUserReward/getId',//根据奖励党员id查询具体奖励信息
    /**
    * @param {partyPunishMgr} 党员处分管理
    */
    delPunishDyAll: api + '/eacUserPunish/deletes',//批量删除处分党员
    getSinglePunishDy: api + '/eacUserPunish/get',//根据处分id查询单个处分党员/77849
    queryPunishDyAll: api + '/eacUserPunish/selectUserPunishment',//查询处分党员集合
    addPunishDy: api + '/eacUserPunish/insert',//新增处分党员
    editPunishDy: api + '/eacUserPunish/update',//修改处分党员
    repealPunishDy: api + '/eacUserPunish/updatePunishUserStatus',//批量撤销处分党员

    /**
     * @param {selectData} 下拉框数据通用接口
     */
    selectUserDict: APISelect2 + '/dj/eac/eacuser/selectUserDict',//党务专员数据下拉框 
    selectmutiple: APISelect2 + '/dj/eac/eacuser/dictList',//党务专员数据下拉框 
    getUserPunish: APISelect2 + '/dj/eac/eacdict/getUserPunish',//党员处罚下拉框
    getUserReward: APISelect2 + '/dj/eac/eacdict/getUserReward',//党员奖励下拉框
    /**
     * @param {upload&export&import} 导入导出上传|查询省市区
     */
    queryCityList: APISelect2 + '/dj/eac/eacdict/dictList',//籍贯省/市/区县查询
    exportUserPunish: APISelect2 + '/eac/common/exportUserPunish',//导出党员处罚信息
    exportUserReward: APISelect2 + '/eac/common/exportUserReward',//导出党员奖励信息
    importUser: APISelect2 + '/dj/eac/eacuser/importUser',//导入党员/党务专员信息
    exportUser: APISelect2 + '/eac/common/exportUser',//导出党员/党务专员/历史党员
}

export default partyDyMgrApi
