let api = '/api/shiro-admin'
let dictCommon = '/api/py-common'
const shiroAdmin = {
    // ------字典数据-------------
    queryDict: dictCommon + '/eacDict/queryByLimit',//分页查询字典?offset=1&limit=10
    dictDetails: dictCommon + '/eacDict/selectOne',//字典详情
    queryDictName: dictCommon + '/eacDict/queryByDictName',//根据字典名称,字典类型查询字典
    addDict: dictCommon + '/eacDict/addEacDict',//新增字典
    editDict: dictCommon + '/eacDict/updateEacDict',//修改字典
    delDict: dictCommon + '/eacDict/delEacDict',//删除字典 delete
    querySelectOne: dictCommon + '/eacDict/selectOne',//字典详情 -根据字典id查询字典
    queryDictId: dictCommon + '/eacDictData/queryByEacDicId',//根据字典id查询字典数据
    deleteAllDict: dictCommon + '/eacDict/remove', //批量删除 post
    addDictData: dictCommon + '/eacDictData/add', //新增字典数据 post
    editDictData: dictCommon + '/eacDictData/update', //修改字典数据 put
    delDictData: dictCommon + '/eacDictData/del', //删除字典数据 delete
    selectDict: dictCommon + '/eacDictData/selectOne', //根据字典数据id查询字典数据 get
    delAllDictData: dictCommon + '/eacDictData/remove',//批量删除
    queryAllEacDictPage: dictCommon + '/eacDict/queryAllEacDictPage',//获取所有字典加子字典分页
    queryChildDictPage: dictCommon + '/eacDict/queryChildDictPage',//根据父ID获取子字典列表
    // ------字典数据 end-------------


    // ------常用组-------------
    groupList: api + '/group/list',//常用组列表
    addCommon: api + '/group/add',//新增常用组?userIds=85385406,85385407,85385408  post
    editCommon: api + '/group/edit',//编辑常用组 post
    delCommon: api + '/group/delete',//删除常用组
    getCommon: api + '/group/selectById',//获取常用组信息
    queryCommonUsers: api + '/group/selectGroupUser',//查询常用组用户
    addCommonUsers: api + '/group/addGroupUser',//添加常用组用户 ?userIds=1,2,3&groupId=9
    delCommonUsers: api + '/group/removeGroupUser',//删除常用组用户 ?userId=1&groupId=10
    // ------常用组 end-------------

    // ------菜单-------------
    resourcesList: api + '/resources/list',//获取菜单列表
    listTree: api + '/resources/listAllTree',//获取菜单树
    listResourceByMenuId: api + '/resources/listResourceByMenuId',//选择菜单树的节点获取下级的菜单列表信息
    resourcesAdd: api + '/resources/add',//菜单新增
    resourcesEdit: api + '/resources/edit',//菜单修改
    resourcesRemove: api + '/resources/remove',//菜单删除，与批量删除
    resourcesGet: api + '/resources/get',//根据id获取菜单信息
    // ------菜单 end-------------

    // ------用户信息-------------
    detailInfo: api + '/eacuser/queryById',//根据用户id查询用户信息 get
    editInfo: api + '/eacuser/updateEacUser',//修改用户信息 put
    // ------用户信息 end-------------


    // ------角色相关 -------------
    roleList: api + '/roles/list',//获取角色列表"
    addRole: api + '/roles/add',//新增角色
    editRole: api + '/roles/edit',//修改角色信息
    delRole: api + '/roles/remove',//删除与批量删除角色
    queryRole: api + '/roles/get',//根据id获取角色信息
    authRole: api + '/roles/saveRoleResources',//为角色添加菜单权限（修改也是这个接口）会进行覆盖
    getAuthRole: api + '/resources/resourcesWithSelected',//获取角色的菜单权限
    saveRoleAndResourcesAndFuncAtSameTime: api +
        "/roles/saveRoleAndResourcesAndFuncAtSameTime",//新增角色
    getUserListByRoldId: api + '/roles/getUserListByRoldId',//获取被分配当前角色的用户
    saveUserRoles: api + '/user/saveUserRoles',
    removeRoleUserByRoldIdAndUserId: api + '/roles/removeRoleUserByRoldIdAndUserId',//删除已分配角色的用户
    getUserRole: api + '/dj/sysuser/getUserRole',//获取用户当前角色
    changeUserRoleID: api + '/user/changeUserRoleID',//获取用户当前角色列表
    getUserRoleList: api + '/dj/sysuser/getUserRoleList',//获取用户当前所有角色
    getVo: api + '/roles/getVo',//根据ID获取菜单详情与菜单下功能
    updateRoleAndResourcesAndFuncAtSameTime: api + '/roles/updateRoleAndResourcesAndFuncAtSameTime',//编辑角色菜单
    getRoleType: api + '/roles/getRoleType',//根据角色id判断当前角色所属类型  1党委，2党总支，3支部，4其他
    // ------角色相关 end-------------

    // ------系统参数 -------------
    detailParams: api + '/systemParameter/selectOne',//根据系统参数id查询参数信息 get
    addSystemParams: api + '/systemParameter/add',//添加系统参数 post
    editSystemParams: api + '/systemParameter/update',//修改系统参数 put
    // ------系统参数 end-------------

    // ------非党员用户 -------------
    userList: api + '/user/list',//获取用户列表
    addUser: api + '/user/add',//新增用户
    editUser: api + '/user/edit',//编辑用户信息
    getUser: api + '/user/get',//获取用户详情
    // ------非党员用户 end-------------
}

export default shiroAdmin
