
const getTreeApi = {
    getTree: '/api/shiro-admin/eacorgan/organTree',//获取组织树
    getAllTypeTree: '/api/py-organ/dj/eac/eacorgan/orgTree',//各类型组织树
    getDwChild: '/api/py-organ/dj/eac/eacorgan/getDwChild',//组织调整树

    // getDwChild: 'http://shiro.sh1.k9s.run:2270/dj/eac/eacorgan/getDwChild',//组织调整树
    // getTree: 'http://shiro.sh1.k9s.run:2270/eacorgan/organTree',//获取组织树
    // getAllTypeTree: 'http://shiro.sh1.k9s.run:2270/dj/eac/eacorgan/orgTree',//各类型组织树
}
export default getTreeApi
