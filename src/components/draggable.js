var form;
    layui.use('form', function(){
       form = layui.form;          
    });
    var dataList=[{
                    name:'单行输入框',
                    value:'inputText',
                    placeholder:'请输入内容',
                    switch:false,
                    OBJECT_NAME:'',
                    OBJECT_FIELD:''

                },{
                    name:'多行输入框',
                    value:'textarea',
                    placeholder:'请输入内容'
                },{
                    name:'下拉列表',
                    value:'select',
                    placeholder:'点击选择'
                },{
                    name:'多选框',
                    value:'checkbox',
                    placeholder:'点击选择'
                },{
                    name:'单选框',
                    value:'radio',
                    placeholder:'点击选择'
                },{
                    name:'开关按钮',
                    value:'switch',
                    placeholder:'点击选择'
                },{
                    name:'日期',
                    value:'formDataTime',
                    placeholder:'点击选择时间'
                },{
                    name:'人员选择',
                    value:'personnel',
                    placeholder:'点击选择人员'
                },{
                    name:'上传图片',
                    value:'upImg',
                    placeholder:'大小不能超过8M'
                },{
                    name:'上传附件',
                    value:'upFolder',
                    placeholder:'大小不能超过8M'
                }]