
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
import '@/common/element-zgh/theme/index.css';
import '@/assets/css/public.css'

import store from './store'//vueX
import http from '@/utils/http'//公共数据请求
import baseData from '@/utils/base'//全局配置参数
import "babel-polyfill";//IE兼容
Vue.prototype.$http = http;
Vue.prototype.$baseData = baseData;
Vue.config.productionTip = false
Vue.use(ElementUI)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
