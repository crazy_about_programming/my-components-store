// @anthor: darker,
// @description: 表格组件基础数据,
// @date: 2020 / 07 / 28
// @remark:

// tableList ---> 表格数据,后端传入
// tableHead ---> 自定义表头
//   表头字段解读:{
//       "prop": "ORG_NAME", ---> 字段标识,与后端传入的值对应 必须
//       "label": "党组织名称", ---> 字段名称,自定义 必须
//       "width": 300, ---> 列宽,自定义 非必须
//       "tooltip": true, ---> 内容过长时是否隐藏,自定义 非必须
//       "type": 0, ---> 自定义列内容,用于扩展
//       "minWidth": 80, ---> 列最小宽度,非必须
//       "sortable": true, ---> 开启排序
//       "isAbled": false, ---> 是否显示,针对不同权限 用于字段排序,字段过滤
//       "index":0, ---> 字段索引,用于排序,可根据该值持久化到数据库,必须
//       "necessity":false  ---> 必须字段,过滤时无法删除该值
//     }

// toolBar ---> 自定义操作列
//   操作列字段解读:{
//       "label": "编辑", ---> 操作按钮名称 必须
//       "icon": "el-icon-edit", ---> 图标 为空时不带图标 非必须
//       "event": 'edit' ---> 触发事件 必须
//     }
//   操作列方法指示:
//         eventFn(obj) {
//           switch (obj.event) {
//             //case 与 toolBar 内容一致 匹配后进行相关操作
//             case 'add': --->该值与toolBar中event值对应
//               // do someThing
//               break;
//             case 'edit':
//               // do someThing
//               break;
//             case 'delete':
//               // do someThing
//               break;
//           };
//         },

// **************************************
// ************ 表格演示数据 **************
// **************************************
export const tableList = () => {
    let data = [{
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "0",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }, {
        "ORG_ID": 10159,
        "D0100": "200123633f9541238d8d213dc234ee93",
        "ORG_CODE": "001001009024",
        "BUSI_CODE": null,
        "ORG_FULLNAME": "20200610组织",
        "ORG_NAME": "20200610组织",
        "ORG_SNAME": null,
        "ORG_LOCATE": null,
        "ORG_TYPE": "621",
        "ORG_TYPE_NAME": "党总支",
        "ORG_LEVEL": 4,
        "P_ORG_ID": "2317",
        "ORG_STATUS": "1",
        "CREATE_DATE": "2020-06-10",
        "LAST_UPDATE": "2020-06-10 09:07:29",
        "ZIP_CODE": null,
        "ORG_CONTACT": "胡立祯",
        "ORG_TEL": "18290027160",
        "FAX_NUM": null,
        "BUILD_DATE": null,
        "BUILD_FILE_NO": null,
        "ORG_DESC": null,
        "CLOSE_DATE": null,
        "CLOSE_FILE_NO": null,
        "CLOSE_DESC": null,
        "ADDR": null,
        "ORDER_SEQ": 99999,
        "CORP_ID": null,
        "LAT": 23.04704,
        "LON": 113.401072,
        "RESERVE1": null,
        "RESERVE2": null,
        "RESERVE3": null,
        "RESERVE4": null,
        "RESERVE5": null,
        "RESERVE6": null,
        "RESERVE7": null,
        "RESERVE8": null,
        "RESERVE9": 2,
        "RESERVE10": null,
        "ORG_PATH": "国务院国资委党委-中共中国第一汽车集团有限公司委员会-中共一汽出行科技有限公司委员会-20200610组织",
        "DEPT_NAME": null,
        "DEPT_NO": null,
        "INTERNATIONAL": null,
        "AREA": null,
        "FOUR_TYPE": null,
        "DEPT_LEADER": null,
        "TERM_LEADERS": null,
        "FINITE": null,
        "FINITE_YEAR": null,
        "PARTY_SECRETARY": "胡立祯",
        "PROVINCE": "26450",
        "CITY": "264504",
        "STREET": "藤县",
        "UNIT_SIYUATION": "与上级党组织相同",
        "LEVEL": "一级",
        "IS_MAIN": "1",
        "ENTERING_CHAPTER": "1",
        "IS_SHOULDERED": "1",
        "IS_VA": "1",
        "ZZ_COUNT": "22",
        "TZ_COUNT": "11",
        "JCZZ_TEXT": "建立党总支部的",
        "ORG_NUMCODE": null,
        "ORG_ATTR": null,
        "ORG_TERR": "广西/梧州市/藤县",
        "CREATE_NUM": null,
        "ORGAN_CODE": null,
        "ORG_TERR_TEXT": "广西梧州市藤县",
        "PROVINCE_TEXT": "广西",
        "CITY_TEXT": "广西梧州",
        "IS_BJ": "在京",
        "IS_JLS": "吉林省内",
        "EAC_CORP_IE": "IE8",
        "EAC_CORP_IE_TEXT": "通信",
        "EAC_CORP_TYPE": "9",
        "EAC_CORP_TYPE_TEXT": null,
        "CORP_NAME": null,
        "CORP_SX_TEXT": null,
        "IE": null,
        "LEVEL1": null,
        "revoke_reason": null,
        "revoke_file": null,
        "revoke_file_name": null,
        "revoke_date": null,
        "IS_MAIN2": "是",
        "ENTERING_CHAPTER2": "是",
        "IS_SHOULDERED2": "是",
        "IS_VA2": "是"
    }];
    return data;
}


export const tableHead = () => {
    let data = [{
        "prop": "ORG_NAME",
        "label": "党组织名称",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWidth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 0,
        "necessity": false,
        "define": false
    },
    {
        "prop": "ORG_TYPE_NAME",
        "label": "组织类别",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 2,
        "necessity": false,
        "define": false
    },
    {
        "prop": "ORG_CONTACT",
        "label": "名字",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": true,
        "index": 1,
        "necessity": true,
        "define": false
    },
    {
        "prop": "ORG_PATH",
        "label": "组织名称",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 4,
        "necessity": false,
        "define": false
    },
    {
        "prop": "ORG_TERR",
        "label": "组织所在地",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 3,
        "necessity": false,
        "define": false
    },
    {
        "prop": "EAC_CORP_IE_TEXT",
        "label": "描述",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": true,
        "index": 5,
        "necessity": false,
        "define": false
    },
    {
        "prop": "EAC_CORP_IE",
        "label": "内核",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": true,
        "index": 7,
        "necessity": false,
        "define": true
    },
    {
        "prop": "IS_JLS",
        "label": "范围",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": true,
        "index": 6,
        "necessity": false,
        "define": false
    },
    {
        "prop": "D0100",
        "label": "编号",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": true,
        "index": 8,
        "necessity": false,
        "define": false
    },
    {
        "prop": "ORDER_SEQ",
        "label": "链表索引",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 9,
        "necessity": false,
        "define": false
    },
    {
        "prop": "ORG_FULLNAME",
        "label": "组织全称",
        "width": '',
        "tooltip": true,
        "type": 0,
        "minWdth": 80,
        "sortable": true,
        "isAbled": false,
        "index": 10,
        "necessity": false,
        "define": false
    },
    {
        "prop": "CREATE_DATE",
        "label": "创建时间",
        "width": 300,
        "tooltip": false,
        "type": 0,
        "minWdth": 100,
        "sortable": true,
        "isAbled": true,
        "index": 11,
        "necessity": false,
        "define": true
    }
    ];
    return data;
}
export const toolBar = () => {
    let data = {
        "width": 280,
        "fixed": 'right',
        "btns": [{
            "label": "编辑",
            "icon": "el-icon-edit",
            "event": 'edit',
            "son": [{
                // "value": 0,
                "label": "再次编辑",
                "event": "editOn",
                // "prop": 'IS_MAIN',
                
            }, {
                // "value": 1,
                "label": "我要编辑",
                "event": "editOn1",
                // "prop": 'IS_MAIN',
               
            }]
        }, {
            "label": "添加",
            "icon": "el-icon-plus",
            "event": 'add'
        }, {
            "label": "删除",
            "icon": "el-icon-delete",
            "event": 'delete'
        }]
    };
    return data;
}


export const footer = () => {
    let data = {
        "background": true,
        "current": 1,
        "align": 'center',
        "pageSize": 10,
        "total": 100,
        "layout": 'total, sizes, prev, pager, next, jumper'
    };
    return data;
}
