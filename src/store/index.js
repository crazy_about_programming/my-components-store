import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    navName: '',//导航位置
    formListCur: []  //预览表单数据
  },
  mutations: {
    loadCurForm(state, data) {
      state.formListCur = data
    },
    navCurEdit(state, payload) {
      state.navName = payload.payload.name
    },
  },
  actions: {
  },
  modules: {
  }
})
