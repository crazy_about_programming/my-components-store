function idPassMust(rule, value, callback) {
  if (value === '') {
    callback(new Error('请输入身份证'));
  } else {
    idPass(rule, value, callback);
  }
}
function checkIdPass(value) {
  var arrExp = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]; // 加权因子
  var arrValid = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2]; // 校验码
  var sum = 0,
    idx;
  for (var i = 0; i < value.length - 1; i++) {
    // 对前17位数字与权值乘积求和
    sum += parseInt(value.substr(i, 1), 10) * arrExp[i];
  }
  // 计算模（固定算法）
  idx = sum % 11;
  return arrValid[idx] == value.substr(17, 1).toUpperCase();
}
function isValidityBrithBy15IdCard(idCard15) {
  var year = idCard15.substring(6, 8);
  var month = idCard15.substring(8, 10);
  var day = idCard15.substring(10, 12);
  var temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
  // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法
  if (temp_date.getYear() != parseFloat(year) || temp_date.getMonth() != parseFloat(month) - 1 || temp_date.getDate() != parseFloat(day)) {
    return false;
  } else {
    return true;
  }
}
const myCommon = {
  //验证身份证
  idPass(rule, value, callback) {
    let regId = /^\d{6}[*]{8}\d{2}[*]{2}$/;
    if (!value) {
      callback(new Error('该身份证号码格式有误！'));
    } else if (regId.test(value)) {
      callback();
    } else {
      var reg = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
      if ((reg.test(value) && checkIdPass(value)) || (value.length == 15 && isValidityBrithBy15IdCard(value))) {
        callback();
      } else {
        callback(new Error('该身份证号码格式有误！'));
      }
    }
  },
  // 手机号码+固话验证
  mobile_call_pass: /^((0\d{2,3}-?\d{7,8})|(1[3456789]\d{9}))$/,
  //手机号码验证
  mobilePass: /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/,
  //验证邮箱
  emailPass: /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/,
}
/**
 * @param {url}  上传地址
 * @param {msg}  提示信息
 * @param {data}  需要导出的字段
 * @param {cb}  回调函数
 */
Vue.prototype.exportExcel = function (url, msg, data, cb) {
  return new Promise((resolve, reject) => {
    this.$confirm(msg, "提示", {
      confirmButtonText: "确定",
      cancelButtonText: "取消",
      type: "warning",
    })
      .then(() => {
        this.$http.post(url, data).then(res => {
          console.log('--导出结果', res)
        })
      })
      .catch(() => {

      });
  });
}


export default myCommon