/****   request.js   ****/
// 导入axios
import axios from 'axios'
import qs from 'qs'

// 使用element-ui Message做消息提醒
import { Message } from 'element-ui';
//1. 创建新的axios实例，
const service = axios.create({
  // 公共接口--这里注意后面会讲
  baseURL: process.env.BASE_API,
  // 超时时间 单位是ms，这里设置了3s的超时时间
  timeout: 6 * 1000
})

// 2.请求拦截器
service.interceptors.request.use(config => {
  //发请求前做的一些处理，数据转化，配置请求头，设置token,设置loading等
  // const token = getCookie('名称');注意使用的时候需要引入cookie方法，推荐js-cookie
  // 
  if (config.type == 'form-data') {
    config.data = qs.stringify(config.data);
    config.headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  } else {
    config.headers = {
      'Content-Type': 'application/json; charset=utf-8'
    }
  }

  let token = localStorage.getItem("token");//token统一传值
  if (token) {
    config.headers = { 'Authorization': token }
  }
  return config
}, error => {
  Promise.reject(error)
})

// 3.响应拦截器
service.interceptors.response.use(response => {

  //登录过期处理
  var data = JSON.stringify(response.data)
  var arr = data.split("登录管理系统");
  if (arr.length > 1) {
    Message.error('请重新登录')
    window.location.href = "./#/login"
  }


  //接收到响应数据并成功后的一些共有的处理，关闭loading等
  if (response.data.status == 500) {
    let message = response.data.message
    Message.error(message)
  }
  return response
}, error => {

  let err = {
    message: ''
  }
  /***** 接收到异常响应的处理开始 *****/
  if (error && error.response) {

    // 1.公共错误处理
    // 2.根据响应码具体处理
    switch (error.response.status) {
      case 400:
        err.message = '错误请求'
        break;
      case 401:
        err.message = '未授权，请重新登录'
        break;
      case 403:
        err.message = '拒绝访问'
        break;
      case 404:
        err.message = '请求错误,未找到该资源'
        break;
      case 405:
        err.message = '请求方法未允许'
        break;
      case 408:
        err.message = '请求超时'
        break;
      case 500:
        err.message = '服务器端出错'
        break;
      case 501:
        err.message = '网络未实现'
        break;
      case 502:
        err.message = '网络错误'
        break;
      case 503:
        err.message = '服务不可用'
        break;
      case 504:
        err.message = '网络超时'
        break;
      case 505:
        err.message = 'http版本不支持该请求'
        break;
      default:
        err.message = `连接错误${error.response.status}`
    }
  } else {

    Message.error('网络错误，请重新登录')
    // window.location.href = "./#/login"
    // // 超时处理
    // if (JSON.stringify(error).includes('timeout')) {
    //   Message.error('服务器响应超时，请刷新当前页')
    // }
    // err.message('网络错误，请重新登录')
  }

  Message.error(err.message)
  /***** 处理结束 *****/
  //如果不需要错误处理，以上的处理过程都可省略
  return Promise.resolve(error.response)
})
//4.导入文件
export default service
